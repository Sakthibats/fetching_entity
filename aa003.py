#imports
import spacy
from spacy.matcher import Matcher
from spacy.matcher import dependencymatcher
from spacy.matcher.dependencymatcher import *
from spacy.tokens import span
from spacy.util import filter_spans
import re

_date_patterns = [
  [{'POS' : 'NUM', 'SHAPE' : {'IN' : ['dd/dd/dddd', 'dddd/dd/dd', 'dd/dd/dd', 'd/dd/dd', 'dd.dd.dddd', 'dddd.dd.dd', 'dd.dd.dd']}}],
  [{'POS' : {'IN' : ['NOUND', 'NUM']}, 'OP' : '+'}, {'LOWER' : {'IN' : ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']}}, {'POS' : 'NUM', 'op' : '?'}],
   [{'LOWER' : {'IN' : ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']}}, {'POS' : {'IN' : ['NOUND', 'NUM']}, 'OP' : '+'}, {'TEXT' : ',', 'OP' : '?'}, {'POS' : 'NUM', 'op' : '?'}],
  [{'LOWER' : {'IN' : ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']}}, {'POS' : {'IN' : ['NOUND', 'NUM']}, 'OP' : '+'}, {'TEXT' : ',', 'OP' : '?'}, {'POS' : 'NUM', 'op' : '?'}],
  [{'ENT_TYPE' : 'DATE'}],
  [{'POS' : {'IN' : ['NOUN', 'NUM']}, 'OP' : '+'}, {'LOWER' : {'IN' : ['am', 'pm']}}, {'POS' : 'NUM', 'op' : '?'}],
   [{'POS' : {'IN' : ['NUM']}, 'OP' : '+', 'POS' : {'IN' : ['NUM']} }],
  ]
# '+', '*', '?', '!'
#text = 'Hi, 23/07/1967'
text = 'IMPORTANT: SPOTS ARE ONLY IN FLIGHT DURING THE WEEKS BELOW:  4/1-4/4,  4/12-4/18,  4/26-5/2,  5/10-5/16,  5/24-5/30  &amp;  6/7-6/13'

_nlp = spacy.load('en_core_web_lg')

def main():
  '''Entry point'''
  processed_spans = []
  raw_spans = []

  matcher = Matcher(_nlp.vocab)
  matcher.add('new_sent', _date_patterns)
  print(text)

  doc = _nlp(text)
  #[print(t.text, t.shape_, t.dep_, t.pos_) for t in doc]
  matches = matcher(doc)
  for m in matches:
    s = doc[m[1] : m[2]]
    raw_spans.append(s)

    processed_spans = filter_spans(raw_spans)

  for s in processed_spans:
    print(s.text)
  return

if __name__ == '__main__':
  print('Starting...')
  main()
  print('Done')


