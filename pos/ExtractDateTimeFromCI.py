#imports
import spacy
from spacy.matcher import DependencyMatcher
from spacy.tokens import span

from patterns_V4 import _first_predicate_patterns, _first_subject_patterns, _second_predicate_patterns, _second_subject_patterns
import dateparser
from dateparser.search import search_dates

#string_id = nlp.vocab.strings[match_id]  

_subject = 'SUBJECT'
_sec_subject = "SEC_SUBJ"
_predicate = 'PREDICATE'

_predicate_exclusions = ['ROOT', 'xcomp', 'punct']
_sent_splitters = [' and ', ' or ']

_nlp = spacy.load('en_core_sci_lg')

_texts = [
  'Early cutoff on 5/27/2021 @ 4 PM dont start airing until 10 am on 5/17/2021'
  ]


__texts = [
  '''cutoff on 5/27/2021 @ 4 PM start airing from 10 am on 5/17/2021''',
  '''Stop airing from 12 PM. start from 1 pm on 2/3/21''',
  '''Stop airing everyday at 3:00 pm''',
  '''Stop airing at 3:00 pm on 7/7  start airing @ 11am 7/8''',
  '''EARLY CUT OFF: SUNDAY, 2/16/20 @ 3:00PM  Early Start 2/17/21 Monday 11AM''',
  '''Don't air from June 1, 2021 to July 1st 2021''',
  '''Don't air between these dates : 2/14/20 - 2/15/20''',
  '''Don't air between these dates : 2/14/20 4PM - 2/15/20 10PM''',
  '''Early stop channel on 6/6/2021 -- 4 PM but do start airing at 10 am on 3/17/2021''',
  '''Early stop channel on 6/6/2021  4 PM but dont start airing till 11 am  2/27/2021''',
  
  '''Stop airing on channel 4 from 12 PM.''',
  ''' dont start airing until 10 am on 5/17/2021 and Early cutoff on 5/27/2021 @ 4 PM''',
  '''Stop airing at 3:00 pm on 7/7''',
  '''Early cutoff on 5/27/2021 @ 4 PM dont start airing until 10 am on 5/17/2021''',
  '''Stop airing at 3:00 pm on 7/7''',
  '''Don't air from June 1, 2021.''',
  '''Don't air: 2/14/20 - 2/15/20'''
  ]

texts_ = [
  '''Early cutoff on 5/27/2021 @ 4 PM dont start airing until 10 am on 5/17/2021''',
  '''Stop airing from 12 PM.''',
  ''' dont start airing until 10 am on 5/17/2021 and Early cutoff on 5/27/2021 @ 4 PM''',
  '''Stop airing at 3:00 pm on 7/7''',
  '''Early cutoff on 5/27/2021 @ 4 PM dont start airing until 10 am on 5/17/2021''',
  '''Stop airing at 3:00 pm on 7/7''',
  '''Don't air from June 1, 2021.''',
  '''Don't air: 2/14/20 - 2/15/20'''
  ]


__texts_ = [
  '''cutoff on 5/27/2021 @ 4 PM start airing from 10 am on 5/17/2021''',
  '''Stop airing from 12 PM. start from 1 pm on 2/3/21''',
  '''Stop airing everyday at 3:00 pm''',
  '''Stop airing at 3:00 pm on 7/7  start airing @ 11am 7/8''',
  '''EARLY CUT OFF: SUNDAY, 2/16/20 @ 3:00PM  Early Start 2/17/21 Monday 11AM''',
  '''Don't air from June 1, 2021 to July 1st 2021''',
  '''Don't air between these dates : 2/14/20 - 2/15/20''',
  '''Don't air between these dates : 2/14/20 4PM - 2/15/20 10PM''',
  '''Early stop channel on 6/6/2021 -- 4 PM but do start airing at 10 am on 3/17/2021''',
  '''Early stop channel on 6/6/2021  4 PM but dont start airing till 11 am  2/27/2021'''
  ]

def GetDate(mainText):
    
  listOfInstrution = []
  listOfInstrution.append(mainText)
  
  '''Entry point'''
  #First initialize the matcher objects. 
  #We need two separate objects to handle the two fragments separately.
  matcher_first_half = DependencyMatcher(_nlp.vocab)
  matcher_second_half = DependencyMatcher(_nlp.vocab)

  #Add the patterns to the objects.
  matcher_first_half.add(_subject, _first_subject_patterns)
  matcher_first_half.add(_predicate, _first_predicate_patterns)

  matcher_second_half.add(_subject, _second_subject_patterns)
  matcher_second_half.add(_predicate, _second_predicate_patterns)

  #Iterate through the sentences.
  for text in listOfInstrution:
    #print(('\n{}').format(text))
    text = text.strip(' ')
    texts = get_sentences(text)
    for sent in texts:
      sentence = sent.strip(' ')
      print(sentence)
      get_details(matcher_first_half, sentence)
      get_details(matcher_second_half, sentence)

  return

def get_details(matcher_obj, text) :
  '''Extracts the relevant details from the specified doc'''
  doc = _nlp(text)
  subject = ''
  subjects = []
  predicates = []

  #print(doc.text)
  #[print(t.text, t.dep_, t.head.text, t.head.dep_) for t in doc]
  matches = matcher_obj(doc)
  for match_id, token_ids in matches:
    string_id = _nlp.vocab.strings[match_id]  
    token_ids = sorted(token_ids)
    phrase = ''
    for i in token_ids:
      phrase = ('{} {}').format(phrase, doc[i])
      if string_id == _predicate:
        predicates.append(i)
      else:
        subjects.append(doc[i].dep_)

    if string_id == _subject and subject == '':
      subject = phrase.strip()

  if subject != '':
    print('Subject: ', subject)
    print('Predicates')
    predicate_text = ''
    for i in predicates:
      token = doc[i]
      #print(token.text, token.dep_, token.head.dep_, token.head.text)
      if token.dep_ not in subjects:
        tree = token.subtree
        for t in tree:
          if t.dep_ not in _predicate_exclusions:
            predicate_text = predicate_text + ' ' + t.text
    
    text =  predicate_text.strip()  
    print(text) 
    para = search_dates(text)
    for x in para:
        date_string =  x[0]
        print(dateparser.parse(date_string).date())
        print(dateparser.parse(date_string).time())
        print(dateparser.parse(date_string))

  return

def get_sentences(text):
  '''Splits the specified text using the coordinating conjunction or noun phrases and returns the sentences'''
  for w in _sent_splitters:
    if text.find(w) > -1:
      text = text.replace(w, ' . ')
      break

  texts = text.split('.')
  return texts


  '''Required for all python programs'''
if __name__ == '__main__':
  print('Starting...')
  newText = 'Early cutoff on 5/27/2021 @ 4 PM dont start airing until 10 am on 5/17/2021'
  newText = 'Early stop channel on 6/6/2021  4 PM but dont start airing till 11 am  2/27/2021'
  
  GetDate(newText)
  print('Done')
