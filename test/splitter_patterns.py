#_verb_patterns = [[{"POS" : {"IN" : ["PART", "AUX", "ADV", "VERB"]}, "OP" : "*"}]]
_action_verb_patterns = [
  [{'POS' :'ADJ', 'DEP' : 'amod'}, {'POS' : 'NOUN', 'DEP' : 'nmod'}],
  [{'POS' : 'VERB', 'DEP' : 'aux'}, {'POS' : 'PART', 'DEP' : 'neg'}, {'POS' : 'VERB', 'DEP' : 'conj'}],
  [{'POS' : 'VERB', 'DEP' : 'aux'}, {'POS' : 'PART', 'DEP' : 'neg'}, {'POS' : 'VERB', 'DEP' : 'ROOT'}],
  [{'POS' : 'VERB', 'DEP' : 'aux'}, {'POS' : 'VERB', 'DEP' : 'conj'}],
  [{'POS' : 'NOUN', 'TAG' : 'NN', 'DEP' : 'compound'}, {'POS' : 'VERB', 'TAG' : 'VBG', 'DEP' : 'nmod'}],
  [{'POS' : 'ADJ', 'DEP' : 'amod'}, {'POS' : 'NOUN', 'DEP' : 'compound'}],
  [{"POS" : 'NOUN', 'DEP' : 'compound'}, {"POS" : "VERB", "DEP" : "nmod", "OP" : "+"}],
  [{'POS' : 'NOUN', 'DEP' : 'nmod' }, {'POS' : 'ADP', 'DEP' : 'case'}, {'POS' : 'NUM', 'DEP' : 'nummod'}],
  [{'POS' : 'VERB', 'TAG' : 'VB', 'DEP' : 'xcomp'}, {'POS' : 'ADP', 'DEP' : 'case'}]
  ]