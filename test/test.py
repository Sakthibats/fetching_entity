import spacy
import random
import typer
from pathlib import Path
from spacy.tokens import DocBin, Doc
from spacy.training.example import Example
# make the factory work
from rel_pipe import make_relation_extractor, score_relations

# make the config work
from rel_model import create_relation_model, create_classification_layer, create_instances


nlp2 = spacy.load("C:/Users/harshit/Downloads/Sub_pred_model-20210906T122643Z-001/Sub_pred_model/model-best-rel")
nlp = spacy.load("C:/Users/harshit/Desktop/coffe-data/coffee-data-demo/flask-relation/model-best-ent")

Text =['''Early cutoff on 5/27/2021 @ 4 PM'''] 
for doc in nlp.pipe(Text, disable=["tagger"]):
   print(f"spans: {[(e.start, e.text, e.label_) for e in doc.ents]}")

for name, proc in nlp2.pipeline:
  print(name)
  doc = proc(doc)
for value, rel_dict in doc._.rel.items():
    for e in doc.ents:
      for b in doc.ents:
        if e.start == value[0] and b.start == value[1]:
          if rel_dict['SUBJECT-PREDICATE'] > 0.3:
            max_key = max(rel_dict, key=rel_dict.get)
            print(rel_dict['SUBJECT-PREDICATE'])
            # print(f" entities: {e.text, b.text} --> predicted relation: {max_key} --> probability {rel_dict[max_key]}")
      break
print(f" entities: {e.text, b.text} --> predicted relation: {max_key} --> probability {rel_dict[max_key]}")